###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @Colby Kagamida <colbykag@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @31 Jan 2021
###############################################################################

CC = gcc
TARGET = animalfarm

all: $(TARGET)

animals.o: animals.c animals.h
	$(CC) -c animals.c

cat.o: animals.h cat.c cat.h
	$(CC) -c cat.c

main.o: animals.h cat.h main.c
	$(CC) -c main.c

animalfarm: animals.o cat.o main.o
	$(CC) -o $(TARGET) animals.o cat.o main.o

	$(info You need to write your own Makefile)
	$(info I know you can do it)
	$(info for now type gcc -o animalfarm *.c)

clean:
	rm -f *.o animalfarm
