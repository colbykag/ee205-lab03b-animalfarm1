///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @31 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdbool.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   //Map the enum Color to a string
   switch(color)
   {
      case BLACK: 
         return "Black";
      case WHITE: 
         return "White";
      case RED: 
         return "Red";
      case BLUE:
         return "Blue";
      case GREEN: 
         return "Green";
      case PINK: 
         return "Pink";
      default: 
         return "None";
   }

   return NULL; // We should never get here
};

/// Decode the enum Gender into strings for printf()
char* genderWord (enum Gender gender) {

   //Map the enum Gender to a string
   switch(gender)
   {
      case FEMALE: 
         return "Female";
      case MALE: 
         return "Male";
      default: 
         return "None";
   }

   return NULL; // We should never get here
};

//Return string for isFixed
char* ynFixed (bool isFixed){
   if(isFixed)
      return "Yes";
   else
      return"No";
}
