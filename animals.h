///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @31 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>

#pragma once

/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
enum Gender {MALE, FEMALE};
enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};

/// Return a string for the name of the color
char* colorName (enum Color color);

//Return string for gender
char* genderWord (enum Gender gender);

//Return string for isFixed
char* ynFixed (bool isFixed);
